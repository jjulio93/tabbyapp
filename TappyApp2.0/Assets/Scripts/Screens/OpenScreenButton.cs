﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenScreenButton : MonoBehaviour
{
    public int screenId;

    public void OnClick()
    {
        ScreenController.instance.ChangeScreen(screenId);
    }
}
