﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Preferences
{
    MusicVolume,
    FXVolume
}

public class PreferencesManager : MonoBehaviour
{

    public static PreferencesManager instance
    {
        get
        {
            if(self == null)
            {
                self = (PreferencesManager)GameObject.FindObjectOfType(typeof(PreferencesManager));
            }
            return self;
        }
    }
    public static PreferencesManager self;

    public float MusicVolume = 1f;
    public float FXVolume = 1f;

    private void Awake()
    {
        if (PlayerPrefs.HasKey("MusicVolume"))
        {
            MusicVolume = PlayerPrefs.GetFloat("MusicVolume");
        }
        else
        {
            PlayerPrefs.SetFloat("MusicVolume",MusicVolume);
        }

        if (PlayerPrefs.HasKey("FXVolume"))
        {
            FXVolume = PlayerPrefs.GetFloat("FXVolume");
        }
        else
        {
            PlayerPrefs.SetFloat("FXVolume", FXVolume);
        }
    }

    public void SetPreference(Preferences preference, float value)
    {
        switch (preference)
        {
            case Preferences.FXVolume:
                {
                    FXVolume = value;
                    PlayerPrefs.SetFloat("FXVolume", FXVolume);
                    break;
                }
            case Preferences.MusicVolume:
                {
                    MusicVolume = value;
                    PlayerPrefs.SetFloat("MusicVolume", MusicVolume);
                    break;
                }
        }
    }

    public float GetPreference (Preferences preference)
    {
        switch (preference)
        {
            case Preferences.FXVolume:
                {
                    return PlayerPrefs.GetFloat("FXVolume");
                }
            case Preferences.MusicVolume:
                {
                    return PlayerPrefs.GetFloat("MusicVolume");
                }
            default:
                {
                    return 0;
                }
        }
    }
}
