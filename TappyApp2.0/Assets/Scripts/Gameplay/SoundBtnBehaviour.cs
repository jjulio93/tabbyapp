﻿using UnityEngine;
using System.Collections;

public class SoundBtnBehaviour : MonoBehaviour
{

    public AudioSource DefaultAudioSource;
    public AudioClip DefaultAduioClip;

    private AudioSource audioSource;

    // Use this for initialization
    void Awake()
    {
        AudioSource tmp_audioSource = GetComponent<AudioSource>();
        if (tmp_audioSource != null)
        {
            this.audioSource = tmp_audioSource;
        }
        else
        {
            this.audioSource = DefaultAudioSource;
        }
        this.audioSource.clip = DefaultAduioClip;
    }

    public void SetAudioClip(AudioClip audioClip)
    {
        this.audioSource.clip = audioClip;
    }

    public void StopAllAudio()
    {
        audioSource.Stop();
    }

    public void OnClick()
    {
        Debug.Log("Audio clicked");
        this.audioSource.Play();
    }
}
