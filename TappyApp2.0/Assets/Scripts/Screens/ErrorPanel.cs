﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ErrorPanel : MonoBehaviour
{
    public Text Message;

    public void setMessage(string message)
    {
        this.Message.text = message;
    }
}
