﻿using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using UnityEngine;

public class ApplicationManager : MonoBehaviour
{
    private const int PROFILE_SCREEN_ID = 1;
    private const int LEVEL_SCREEN_ID = 0;

    private GameData gamedata;

    public static ApplicationManager instance
    {
        get
        {
            if (self == null)
            {
                self = (ApplicationManager)GameObject.FindObjectOfType(typeof(ApplicationManager));
            }
            return self;
        }
    }

    private static ApplicationManager self;
    private void Awake()
    {
        InitApplication();
    }

    private void InitApplication()
    {
        gamedata = StorageManager.LoadData();
        if (gamedata == null)
        {
            Debug.Log("NewGame");
            gamedata = new GameData();

            ScreenController.instance.ChangeScreen(PROFILE_SCREEN_ID);
        }
        else
        {
            ScreenController.instance.ChangeScreen(LEVEL_SCREEN_ID);
        }

    }

    public GameData getGameData()
    {
        return gamedata;
    } 

    public void setGameData(GameData gamedata)
    {
        this.gamedata = gamedata;
    }

    public void Save()
    {
        StorageManager.SaveData(gamedata);
    }
}
