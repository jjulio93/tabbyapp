﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

[System.Serializable]
public class ProfileData
{
    public int PictureID;
    public string PlayerName;
    public int Age;
    public bool Gender;  // false=Male, true=Female

    public ProfileData()
    {
        this.PictureID = 0;
        this.PlayerName = "";
        this.Age = 10;
        this.Gender = false;
    }

}

[System.Serializable]
public class GameData
{
    public int[] LevelProgress; // 0 means unstarted, 1-5 Excercise Progress.
    public ProfileData Profile;
    
    public GameData()
    {
        LevelProgress = new int[10];
        Profile = new ProfileData();
    }
}
