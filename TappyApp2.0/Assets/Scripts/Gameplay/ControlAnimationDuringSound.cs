﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlAnimationDuringSound : MonoBehaviour
{
    public AudioSource AudioSource;
    public Animator Animator;

    // Update is called once per frame
    void Update()
    {
        Animator.SetBool("Playing", AudioSource.isPlaying);
    }
}
