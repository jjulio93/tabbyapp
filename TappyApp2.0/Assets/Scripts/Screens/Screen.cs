﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Screen : MonoBehaviour
{

    public Screen[] panels;

    public void Show()
    {
        gameObject.SetActive(true);
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }

    public void ShowPanel(int panel)
    {
        panels[panel].Show();
    }

    public void HidePanel(int panel)
    {
        panels[panel].Hide();
    }

    public void HideAllPanels()
    {
        for(int i=0; i<panels.Length; i++)
        {
            panels[i].Hide();
        }
    }

}
