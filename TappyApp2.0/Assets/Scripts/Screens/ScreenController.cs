﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ScreenController : MonoBehaviour
{

    public static ScreenController instance
    {
        get
        {
            if (self == null)
            {
                self = FindObjectOfType<ScreenController>();
            }
            return self;
        }
    }
    private static ScreenController self;

    public Screen[] screens;

    private int currentScreen=0;

    public void ChangeScreen(int screen)
    {
        screens[currentScreen].Hide();
        screens[screen].Show();
    }

    public Screen getCurrentScreen()
    {
        return screens[currentScreen];
    }

}
