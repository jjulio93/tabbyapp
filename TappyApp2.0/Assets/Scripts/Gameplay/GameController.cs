﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using UnityEngine;
using Random = UnityEngine.Random;

[Serializable]
public class GameConfiguration
{
    public int Rounds;
    public Card[] availableCards;
}

public class GameController : MonoBehaviour
{

    public static GameController instance
    {
        get
        {
            if (self == null)
            {
                self = FindObjectOfType<GameController>();
            }
            return self;
        }
    }
    private static GameController self;

    public GameConfiguration config;
    public CardBehaviour card1;
    public CardBehaviour card2;
    public SoundBtnBehaviour soundBtn;
    public AudioSource GameAudio;

    private int correctAnswerCounter;
    private int currentRound;
    

    private int[] currentCards;
    private int selectedCard;

    private DateTime startTime;
    private DateTime endTime;

    // Start is called before the first frame update
    void Start()
    {
      
    }

    public void RestartGame()
    {
        soundBtn.StopAllAudio();
        init();
    }

    public bool SubmitAnswer(int card)
    {
        if(card == selectedCard)
        {
            correctAnswerCounter++;
            moveToNextRound();
            return true;
        }
        else
        {
            return false;
        }
    }

    private void init()
    {
        correctAnswerCounter = 0;
        currentRound = 0;
        GameAudio.Play();
        startTime = DateTime.Now;
        moveToNextRound();
    }

    private void createRound()
    {
        selectPair();
        card1.SetCard(config.availableCards[currentCards[0]].CardImage,0);
        card2.SetCard(config.availableCards[currentCards[1]].CardImage,1);
        soundBtn.SetAudioClip(config.availableCards[currentCards[selectedCard]].CardAudioClip);
    }

    private void endGame()
    {
        Debug.Log("Game Ended");
        soundBtn.StopAllAudio();
        GameAudio.Stop();
        endTime = DateTime.Now;
        EndGamePanelBehaviour.instance.Show(correctAnswerCounter,config.Rounds,startTime,endTime);
    }

    private void moveToNextRound()
    {
        if(currentRound == config.Rounds)
        {
            endGame();
            return;
        }
        currentRound++;
        createRound();
    }

    private void selectPair()
    {
        this.currentCards = new int[2];

        int pos1 = Random.Range(0, config.availableCards.Length);
        int pos2 = Random.Range(0, config.availableCards.Length);

        if (pos1 == pos2)
        {
            pos2++;
            if (pos2 >= config.availableCards.Length)
            {
                pos2 = 0;
            }
        }

        currentCards[0] = pos1;
        currentCards[1] = pos2;

        this.selectedCard = Random.Range(0, currentCards.Length);
    }

}
