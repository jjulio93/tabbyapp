﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ProfilePicture : MonoBehaviour
{

    public Sprite[] Images;

    private Image ProfileImage;

    private void Awake()
    {
        ProfileImage = GetComponent<Image>();
    }

    // Start is called before the first frame update
    void OnEnable()
    {

        GameData profileData = ApplicationManager.instance.getGameData();
        if (profileData != null)
        {
            ProfileImage.sprite = Images[profileData.Profile.PictureID];
        }
    }

    
}
