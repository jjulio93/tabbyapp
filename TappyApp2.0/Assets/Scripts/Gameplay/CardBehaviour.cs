﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEngine.UI;

public class CardBehaviour : MonoBehaviour
{

    private const string WRONG_ANSWER_TRIGGER = "Pressed";

    private int value;
    private Image image;
    private Animator anim;

    // Start is called before the first frame update
    void Awake()
    {
        image = GetComponentInChildren<Image>();
        anim = GetComponent<Animator>();
    }

    public void SetCard(Sprite sprite, int value)
    {
        image.sprite = sprite;
        this.value = value;
    }

    public void OnClick()
    {
        Debug.Log("Card Clicked");
        bool isAnswer = GameController.instance.SubmitAnswer(this.value);
        if (!isAnswer)
        {
            anim.SetTrigger(WRONG_ANSWER_TRIGGER);
        }
    }
}
