﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConfigPanel : MonoBehaviour
{

    public ToggleButtonGraphic MusicToggle;
    public ToggleButtonGraphic FXToggle;

    private bool MusicMuted = false;
    private bool FXMuted = false;

    private void OnEnable()
    {
        float music_volume = PreferencesManager.instance.GetPreference(Preferences.MusicVolume);
        float fx_volume = PreferencesManager.instance.GetPreference(Preferences.FXVolume);

        Debug.Log(music_volume + ":" + fx_volume);

        if (PreferencesManager.instance.GetPreference(Preferences.MusicVolume) > 0f)
        {
            MusicMuted = false;
        }
        else
        {
            MusicMuted = true;
        }

        if (PreferencesManager.instance.GetPreference(Preferences.FXVolume) > 0f)
        {
            FXMuted = false;
        }
        else
        {
            FXMuted = true;
        }

        Debug.Log(MusicMuted + ":" + FXMuted);

        MusicToggle.setValue(MusicMuted);
        FXToggle.setValue(FXMuted);
    }

    public void MusicButtonOnclick()
    {
        MusicMuted = !MusicMuted;
        float value = MusicMuted ? 0f : 1f;
        Debug.Log("Music Muted:" + MusicMuted + ":" + value);
        PreferencesManager.instance.SetPreference(Preferences.MusicVolume, value);
    }

    public void FXButtonOnClick()
    {
        FXMuted = !FXMuted;
        float value = FXMuted ? 0f : 1f;
        Debug.Log("FX Muted:" + FXMuted + ":" + value);
        PreferencesManager.instance.SetPreference(Preferences.FXVolume, value);
    }


}
