﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundPreferenceController : MonoBehaviour
{

    public Preferences preferenceControl;

    private AudioSource controlledSource;

    // Start is called before the first frame update
    void Start()
    {
        this.controlledSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        int currentValue = (int) Math.Floor(PreferencesManager.instance.GetPreference(preferenceControl));
        if (currentValue <= 0)
        {
            controlledSource.mute = true;
        }
        else
        {
            controlledSource.mute = false;
        }
    }
}
