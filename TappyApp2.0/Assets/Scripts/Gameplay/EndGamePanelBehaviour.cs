﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndGamePanelBehaviour : MonoBehaviour
{

    public Text scoreText;
    public Text timeText;

    public static EndGamePanelBehaviour instance
    {
        get
        {
            if(self == null)
            {
               self = FindObjectOfType<EndGamePanelBehaviour>();
            }
            return self;
        }
    }
    private static EndGamePanelBehaviour self;

    public Button replayButton;

    // Start is called before the first frame update
    void Awake()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Show(int score, int rounds, DateTime startTime, DateTime endTime)
    {
        this.scoreText.text = score.ToString() + "/" + rounds.ToString();
        TimeSpan roundDuration = endTime - startTime;
        this.timeText.text = new TimeSpan(roundDuration.Hours, roundDuration.Minutes, roundDuration.Seconds).ToString("c");

        transform.GetChild(0).gameObject.SetActive(true);
    }

    public void Hide()
    {
        transform.GetChild(0).gameObject.SetActive(false);
    }

    public void replayBtnOnClick()
    {
        GameController.instance.RestartGame();
        Hide();
    }
}
