﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.ExceptionServices;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class StorageManager
{
    private static string FileLocation = "/Game.dat";

    public static void SaveData(GameData data)
    {
        if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
            System.Environment.SetEnvironmentVariable("MONO_REFLECTION_SERIALIZER", "yes");
        }

        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + FileLocation;
        FileStream fileStream = new FileStream(path, FileMode.Create);
        formatter.Serialize(fileStream, data);
        fileStream.Close();
    }

    public static GameData LoadData()
    {

        if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
            System.Environment.SetEnvironmentVariable("MONO_REFLECTION_SERIALIZER", "yes");
        }

        string path = Application.persistentDataPath + FileLocation;
        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream fileStream = new FileStream(path, FileMode.Open);

            GameData data = formatter.Deserialize(fileStream) as GameData;

            fileStream.Close();
            return data;
        }
        else
        {
            Debug.LogError("Game Data file not found");
            return null;
        }

    }
}
