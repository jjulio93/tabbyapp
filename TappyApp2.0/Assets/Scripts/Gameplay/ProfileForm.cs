﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProfileForm : MonoBehaviour
{

    private const int LEVEL_SCREEN_ID = 0;
    private Color PICTURE_NOT_SELECTED_COLOR = Color.grey;
    private Color PICTURE_SELECTED_COLOR = Color.white;
    private Color GENDER_SELECTED_COLOR = new Color(162f / 255f, 117f / 255f, 234f / 255f);
    private Color GENDER_NOT_SELECTED_COLOR = Color.white;

    public Image[] pictures;
    public Image[] genderButtons;

    public InputField NameField;
    public InputField AgeField;
    public bool gender;
    public int PictureId;

    public ErrorPanel ErrorPanel;
    public Screen ThisScreen;

    private void Awake()
    {
        PicuteButtonOnClick(PictureId);
    }

    private void OnEnable()
    {
        genderButtons[0].color = GENDER_NOT_SELECTED_COLOR;
        genderButtons[1].color = GENDER_NOT_SELECTED_COLOR;
    }

    public void Submit()
    {
        if (validateFields())
        {
            GameData data = ApplicationManager.instance.getGameData();
            data.Profile.PlayerName = NameField.text;
            data.Profile.Age = int.Parse(AgeField.text);
            data.Profile.Gender = gender;
            data.Profile.PictureID = PictureId;
            ApplicationManager.instance.setGameData(data);
            ApplicationManager.instance.Save();
            GetComponent<Screen>().Hide();
            ScreenController.instance.ChangeScreen(LEVEL_SCREEN_ID);
        }
    }
    
    private bool validateFields()
    {
        if(NameField.text=="" || AgeField.text == "")
        {
            ErrorPanel.setMessage("El nombre y la edad no pueden estar vacios");
            ThisScreen.ShowPanel(0);
            return false;
        }
        if(int.Parse(AgeField.text) <= 0)
        {
            ErrorPanel.setMessage("La edad no puede ser menor o igual a cero");
            ThisScreen.ShowPanel(0);
            return false;
        }
        return true;
    }

    public void GenderButtonOnClick(bool gender)
    {
        this.gender = gender;
        genderButtons[0].color = gender ? GENDER_NOT_SELECTED_COLOR : GENDER_SELECTED_COLOR;
        genderButtons[1].color = gender ? GENDER_SELECTED_COLOR : GENDER_NOT_SELECTED_COLOR;
    }

    public void PicuteButtonOnClick(int id)
    {
        this.PictureId = id;
        for (int i = 0; i < pictures.Length; i++)
        {
            if(i == id)
            {
                pictures[i].color = PICTURE_SELECTED_COLOR;
            }
            else
            {
                pictures[i].color = PICTURE_NOT_SELECTED_COLOR;
            }
        }
    }

}
