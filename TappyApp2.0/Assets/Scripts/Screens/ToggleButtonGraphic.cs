﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleButtonGraphic : MonoBehaviour
{

    public Image TargetImage;
    public Sprite[] ToggleSprites;
    public bool value = false;

    public void setValue(bool value)
    {
        this.value = value;
        setImage();
    }

    public void OnClick()
    {
        value = !value;
        setImage();
    }

    private void setImage()
    {
        int spriteId = value ? 1 : 0;
        TargetImage.sprite = ToggleSprites[spriteId];
    }
}
